
const FIRST_NAME = "Diana Gabriela";
const LAST_NAME = "Feighel";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
class Employee {
    constructor(name, surname, salary){
        this.name=name;
        this.surname=surname;
        this.salary=salary;
    }
    
    getDetails(){
        return this.name+' '+this.surname+
        ' '+this.salary;
    }
}

class SoftwareEngineer extends Employee {
    constructor(name, surname, salary, experience='JUNIOR'){
        super(name,surname,salary);
        this.experience = experience;
    }
    applyBonus(){
        switch(this.experience){
            case 'MIDDLE' : return this.salary*1.15;
            case 'SENIOR' : return this.salary*1.2;
            default : return this.salary*1.1;
        }
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

